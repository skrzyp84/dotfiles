set shell=/bin/bash

set nocompatible " be iMproved, required
filetype off " required

"$VIM/vimfiles,
"/usr/share/vim/vimfiles
set runtimepath=~/.vim,$VIMRUNTIME

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Plugins

" All of your Plugins must be added before the following line
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
Plugin 'jelera/vim-javascript-syntax'
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'shougo/neocomplcache.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'junegunn/fzf'
Plugin 'heavenshell/vim-jsdoc'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'dhruvasagar/vim-table-mode'
Plugin 'rust-lang/rust.vim'
Plugin 'cespare/vim-toml'
Plugin 'uarun/vim-protobuf'
Plugin 'Yggdroot/indentLine'
Plugin 'flazz/vim-colorschemes'
Plugin 'fatih/vim-go'
Plugin 'scrooloose/nerdtree'

call vundle#end() " required
filetype plugin indent on " required

"open NERDTree
autocmd vimenter * NERDTree
"jum to left window
autocmd VimEnter * wincmd l


"
" Brief help
" :PluginList - lists configured plugins
" :PluginInstall - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
let g:neocomplcache_enable_at_startup = 1
let g:airline_theme='dark_minimal'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

let g:ctrlp_map = ''
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_prompt_mappings = {
\ 'AcceptSelection("e")': [''],
\ 'AcceptSelection("t")': ['', '<2-LeftMouse>'],
\ }
let g:indentLine_char = '┆'
let g:indentLine_color_term = 238


" neocomplcache
let g:neocomplcache_enable_at_startup = 1 " 起動時に有効化
inoremap neocomplcache#smart_close_popup() . "\"
inoremap pumvisible() ? "\" : "\"


syntax on

set relativenumber
set number

set fileencoding=utf-8
set encoding=utf-8
set cursorline

set tabstop=4
set expandtab
set smarttab
set shiftwidth=4
set softtabstop=4
set t_Co=256

colorscheme harlequin 
hi CursorLine cterm=NONE ctermbg=000000

nnoremap gT <S-h> gT
nnoremap gt <S-l> gt
nnoremap ZX :qa<CR>

map <Tab> <C-W>w
"map <S-Tab> <C-W>p
map <S-Tab> <C-W><Left>

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Tab navigation like Firefox.
" nnoremap <C-S-tab> :tabp<CR>
" nnoremap <C-tab>   :tabn<CR>
" nnoremap <C-t>     :tabnew<CR>
" inoremap <C-S-tab> <Esc>:tabp<CR>i
" inoremap <C-tab>   <Esc>:tabn<CR>i
" inoremap <C-t>     <Esc>:tabnew<CR>

